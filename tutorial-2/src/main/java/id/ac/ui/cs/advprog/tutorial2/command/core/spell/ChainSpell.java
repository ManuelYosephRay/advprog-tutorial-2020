
package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    ArrayList<Spell> arrSpell;
    public ChainSpell(ArrayList<Spell> spell){
        this.arrSpell = spell;
    }
    @Override
    public void cast() {
        for(int i=0; i<arrSpell.size() ;i++){
            arrSpell.get(i).cast();
        }
    }

    @Override
    public void undo() {
        for(int i=0; i<arrSpell.size() ;i++) {
            arrSpell.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
