package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.Familiar;

public class FamiliarSealSpell extends FamiliarSpell {
    public FamiliarSealSpell(Familiar familiar) {
        super(familiar);
    }
    // TODO: Complete Me

    @Override
    public String spellName() {
        return familiar.getRace() + ":Seal";
    }

    public void cast(){
        familiar.seal();
    }
}
