package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
    String name;
    String role;
    ArrayList<Member> arrayChild = new ArrayList<>();
    public PremiumMember(String wati, String gold_merchant) {
        this.name = wati;
        this.role = gold_merchant;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getRole() {
        return role;
    }

    @Override
    public void addChildMember(Member member) {
        arrayChild.add(member);
    }

    @Override
    public void removeChildMember(Member member) {
        arrayChild.remove(member);
    }

    @Override
    public List<Member> getChildMembers() {
        return arrayChild;
    }
    //TODO: Complete me
}
