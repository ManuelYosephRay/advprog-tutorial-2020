package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class ChaosUpgrade extends Weapon {

    Weapon weapon;
    int valueUp;

    public ChaosUpgrade(Weapon weapon) {

        this.weapon= weapon;
        valueUp = new Random().nextInt(6) +50;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 50-55 ++
    @Override
    public int getWeaponValue() {
        return valueUp + weapon.getWeaponValue();
        //TODO: Complete me
    }

    @Override
    public String getDescription() {
        return "Chaos " + weapon.getDescription();
        //TODO: Complete me
    }
}
