package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class OrdinaryMember implements Member {
    String name;
    String role;
    ArrayList<Member> childArray = new ArrayList<>();
    public OrdinaryMember(String name, String role) {
        this.name = name;
        this.role = role;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getRole() {
        return role;
    }

    @Override
    public void addChildMember(Member member) {
        childArray.add(member);
    }

    @Override
    public void removeChildMember(Member member) {
        childArray.remove(member);
    }

    @Override
    public List<Member> getChildMembers() {
        return childArray;
    }
    //TODO: Complete me
}
