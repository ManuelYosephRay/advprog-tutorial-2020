package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RawUpgrade extends Weapon {

    Weapon weapon;
    int valueUp;

    public RawUpgrade(Weapon weapon) {
        this.valueUp = new Random().nextInt(6) +5;
        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 5-10 ++
    @Override
    public int getWeaponValue() {
        return valueUp + weapon.getWeaponValue();
        //TODO: Complete me
    }

    @Override
    public String getDescription() {
        return "Raw " + weapon.getDescription();
        //TODO: Complete me
    }
}
