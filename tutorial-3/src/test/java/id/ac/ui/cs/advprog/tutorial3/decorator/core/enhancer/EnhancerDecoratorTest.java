package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;



public class EnhancerDecoratorTest {

    Weapon weapon1;
    Weapon weapon2;
    Weapon weapon3;
    Weapon weapon4;
    Weapon weapon5;
    @Test
    public void testAddWeaponEnhancement(){

        weapon1 = EnhancerDecorator.CHAOS_UPGRADE.addWeaponEnhancement(weapon1);
        weapon2 = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(weapon2);
        weapon3 = EnhancerDecorator.REGULAR_UPGRADE.addWeaponEnhancement(weapon3);
        weapon4 = EnhancerDecorator.RAW_UPGRADE.addWeaponEnhancement(weapon4);
        weapon5 = EnhancerDecorator.UNIQUE_UPGRADE.addWeaponEnhancement(weapon5);

        int weaponValue1 = weapon1.getWeaponValue();
        int weaponValue2 = weapon2.getWeaponValue();
        int weaponValue3 = weapon3.getWeaponValue();
        int weaponValue4 = weapon4.getWeaponValue();
        int weaponValue5 = weapon5.getWeaponValue();

        assertTrue(weaponValue1 >= 50 && weaponValue1 <= 55);
        assertTrue(weaponValue2 >= 15 && weaponValue2 <= 20);
        assertTrue(weaponValue3 >= 1 && weaponValue3 <= 5);
        assertTrue(weaponValue4 >= 5 && weaponValue4 <= 10);
        assertTrue(weaponValue5 >= 10 && weaponValue5 <= 15);

        //TODO: Complete me
    }

}
