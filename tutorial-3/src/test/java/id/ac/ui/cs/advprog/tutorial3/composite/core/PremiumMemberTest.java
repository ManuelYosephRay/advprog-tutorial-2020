package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        assertEquals("Wati", member.getName());
        //TODO: Complete me
    }

    @Test
    public void testMethodGetRole() {
        assertEquals("Gold Merchant", member.getRole());
        //TODO: Complete me
    }

    @Test
    public void testMethodAddChildMember() {
        Member testadd = new PremiumMember("Jajang", "Sudrajat");
        member.addChildMember(testadd);
        assertEquals(1, member.getChildMembers().size());
        //TODO: Complete me
    }

    @Test
    public void testMethodRemoveChildMember() {
        Member remove = new PremiumMember("Jajang", "Sudrajat");
        member.addChildMember(remove);
        member.removeChildMember(remove);
        assertEquals(0, member.getChildMembers().size());

        //TODO: Complete me
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        member.addChildMember(new PremiumMember("Jajang", "Premium"));
        member.addChildMember(new PremiumMember("Atep", "Premium"));
        member.addChildMember(new PremiumMember("Hariono", "Premium"));
        member.addChildMember(new PremiumMember("Sudrajat", "Premium"));
        assertEquals(3, member.getChildMembers().size());

        //TODO: Complete me
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        Member master = new PremiumMember("RidwanKamil", "Master");
        master.addChildMember(new PremiumMember("Jajang", "Premium"));
        master.addChildMember(new PremiumMember("Atep", "Premium"));
        master.addChildMember(new PremiumMember("Hariono", "Premium"));
        master.addChildMember(new PremiumMember("Sudrajat", "Premium"));
        assertEquals(4, master.getChildMembers().size());

        //TODO: Complete me
    }
}
