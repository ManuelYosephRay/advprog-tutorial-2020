package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Jajang", "Sudrajat");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        //TODO: Complete me
        guild.addMember(guildMaster,new OrdinaryMember("Jajang","Sudrajat"));
        assertEquals(2,guild.getMemberList().size());
    }

    @Test
    public void testMethodRemoveMember() {
        //TODO: Complete me
        guild.addMember(guildMaster,new OrdinaryMember("Jajang","Sudrajat"));
        guild.removeMember(guildMaster,guild.getMember("Jajang","Sudrajat"));
        assertEquals(1,guild.getMemberList().size());
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("S", "Uchiha");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        Member child = new OrdinaryMember("Jajang","Sudrajat");
        guild.addMember(guildMaster,child);
        assertEquals(child,guild.getMember("Jajang","Sudrajat"));

        //TODO: Complete me
    }
}
