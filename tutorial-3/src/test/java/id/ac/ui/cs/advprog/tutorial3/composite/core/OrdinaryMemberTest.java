package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrdinaryMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new OrdinaryMember("Nina", "Merchant");
    }

    @Test
    public void testMethodGetName() {
        assertEquals("Nina", member.getName());
        //TODO: Complete me
    }

    @Test
    public void testMethodGetRole() {
        assertEquals("Merchant", member.getRole());//TODO: Complete me
    }

    @Test
    public void testMethodAddRemoveChildMemberDoNothing() {
        Member addremove = new OrdinaryMember("Jajang", "Premium");
        member.addChildMember(addremove);
        assertEquals(0, member.getChildMembers().size());
        member.removeChildMember(addremove);
        assertEquals(0, member.getChildMembers().size());

        //TODO: Complete me
    }
}
